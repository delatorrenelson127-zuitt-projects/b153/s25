let http  = require('http')

const server = http.createServer(function(request, response){
    response.writeHead(200, {'Content-Type': 'text/plain'})


    response.end('Server Routes')
})

const port = 4000;

server.listen(port)

console.log(`Server running in ${port}`)